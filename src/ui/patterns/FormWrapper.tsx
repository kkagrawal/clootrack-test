import { yupResolver } from '@hookform/resolvers/yup'
import { forwardRef } from 'react'
import { useForm } from 'react-hook-form'

function FormWrapper({ children, validationSchema, submitHandler, ...props }: any, ref: any) {
  const formOptions = { resolver: yupResolver(validationSchema) }
  const { register, handleSubmit, watch, formState, setValue } = useForm(formOptions)
  const { errors } = formState

  return (
    <form ref={ref} onSubmit={handleSubmit(submitHandler)} {...props}>
      {children(register, errors, setValue, watch)}
    </form>
  )
}

export default forwardRef(FormWrapper)
