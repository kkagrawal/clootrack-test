import { withLayout } from '@/app/HOC/withLayout'
import { RootState } from '@/app/store'
import chartsApi from '@/core/charts/api/charts.api'
import { chartActions } from '@/core/charts/chart.slice'
import ChartManager from '@/core/charts/ChartManager'
import DefaultLayout from '@/layouts/Default.layout'
import { Box, CircularProgress, Tab, Typography } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import TabContext from '@mui/lab/TabContext'
import TabList from '@mui/lab/TabList'
import TabPanel from '@mui/lab/TabPanel'
import useEffectOnce from 'react-use/lib/useEffectOnce'
import { useState } from 'react'
import { SyntheticEvent } from 'react'

function Index() {
  const dispatch = useDispatch()
  const [loading, setLoading] = useState(true)

  const chartData = useSelector((state: RootState) => state.chart.data)
  const [index, setIndex] = useState('1')

  const handleChange = (event: SyntheticEvent, newValue: string) => {
    setIndex(newValue)
  }

  useEffectOnce(() => {
    chartsApi
      .fetchAllData()
      .then((data) => {
        dispatch(chartActions.addData(data))
        setLoading(false)
      })
      .catch((err) => {
        console.log(err)
        setLoading(false)
        alert(err)
      })
  })

  return (
    <main style={{ textAlign: 'center' }}>
      <Typography sx={{ textAlign: 'center', padding: 3 }} variant="h3">
        Cooltrack Charts
      </Typography>
      {loading ? (
        <CircularProgress />
      ) : (
        <Box sx={{ width: '80%', marginX: 5 }}>
          <TabContext value={index}>
            <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
              <TabList onChange={handleChange} variant="scrollable" scrollButtons="auto" aria-label="Chart">
                {chartData &&
                  chartData.map((data, index) => (
                    <Tab key={'chart_' + data.type + index} label={'Chart ' + (index + 1)} value={index + ''} />
                  ))}
              </TabList>
            </Box>

            {chartData &&
              chartData.map((data, index) => (
                <TabPanel key={'tab-content' + index} value={index + ''}>
                  <ChartManager index={index} data={data}></ChartManager>
                </TabPanel>
              ))}
          </TabContext>
        </Box>
      )}
    </main>
    // <Chart type={chartData[index].type} data={chartData[index].elements}></Chart>
  )
}

export default withLayout(DefaultLayout, Index)
