import { Typography } from '@mui/material'
import { Card } from '@mui/material'
import { useState } from 'react'
import { useDispatch } from 'react-redux'
import { chartActions } from './chart.slice'
import Chart from './components/Chart'
import DataEditor from './components/DataEditor'

function ChartManager(props: any) {
  const [data, setData] = useState(props.data.elements)
  const [type, setType] = useState(props.data.type)

  const dispatch = useDispatch()

  function save() {
    dispatch(chartActions.updateData({ index: props.index, data: { elements: data, type } }))
  }

  return (
    <Card style={{ maxWidth: 550 }}>
      <Typography gutterBottom sx={{ paddingTop: 1 }} variant="h5">
        {type} Chart
      </Typography>
      <DataEditor values={props.data} setData={setData} setType={setType} save={save}></DataEditor>
      <Chart type={type ?? props.data.type} data={data ?? props.data.elements} />
    </Card>
  )
}

export default ChartManager
