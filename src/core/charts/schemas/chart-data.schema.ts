import * as Yup from 'yup'

export const dataSchema = Yup.object().shape({
  data: Yup.string().matches(/^[0-9\s,]+$/),
})
