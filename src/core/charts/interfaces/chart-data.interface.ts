export interface IChartData {
  name: string
  value: string | number
}
