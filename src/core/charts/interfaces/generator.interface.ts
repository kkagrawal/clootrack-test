import DataModel from '../models/data.model'

export interface IGenerator {
  getDataModel(data: any): DataModel

  options(chartData: DataModel): any
}
