// Change `type` property to enum later
export default class DataModel {
  public columns: any[] = []
  public type: string

  constructor(type: string, public values: any[], columns?: any[]) {
    if (!columns) {
      columns = Array.from(values.keys())
      // } else if(this.columns.length < this.values.length) {
    }
    this.columns = columns
    this.type = type.toLowerCase()
  }

  public static fromArray(type: string, values: any) {
    return new DataModel(type, values, values)
  }

  public static fromObject(type: string, obj: any) {
    return new DataModel(type, Object.values(obj), Object.keys(obj))
  }

  public static fromCollection(type: string, collection: { name: string; value: any }[]) {
    const values: any[] = []
    const columns: any[] = []
    collection.forEach((el) => {
      values.push(el.value)
      columns.push(el.name)
    })

    return new DataModel(type, values, columns)
  }

  toObject() {
    const a: any = {}
    this.values.forEach((value: any, index: number) => {
      if (this.columns && this.columns.length > index) {
        a[this.columns[index]] = value
      } else {
        a[index] = value
      }
    })

    return a
  }

  toCollection() {
    const a: any[] = []
    for (let i = 0; i < this.values.length; i++) {
      a.push({
        name: this.columns[i],
        value: this.values[i] ?? null,
      })
    }
    return a
  }
}
