import { createSlice } from '@reduxjs/toolkit'

import { IChartSliceState } from './interfaces/chart-slice.interface'

const initialState: IChartSliceState = {
  data: [],
}

const chartSlice = createSlice({
  name: 'chart',
  initialState,
  reducers: {
    addData(state, { payload }: any) {
      if (payload) {
        state.data = payload
      }
    },

    updateData(state, { payload }: any) {
      if (payload) {
        state.data[payload.index] = payload.data
      }
    },
  },
})

export const chartActions = chartSlice.actions
export default chartSlice.reducer
