import EChartsReact from 'echarts-for-react'

import generators from '../generators'

function Chart(props: any) {
  const type = props.type.toLowerCase()
  const generator = generators[type]
  const chartData = generator.getDataModel(props.data)
  return <EChartsReact option={generator.options(chartData)} />
}

export default Chart
