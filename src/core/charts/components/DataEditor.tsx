import FormWrapper from '@/ui/patterns/FormWrapper'
import { Autocomplete, Box, Button, TextField } from '@mui/material'
import { useState } from 'react'
import useDebounce from 'react-use/lib/useDebounce'
import { dataSchema } from '../schemas/chart-data.schema'

const typeOptions = ['Bar', 'Pie']

export default function (props: any) {
  // const [val] = useState()
  // console.log(props.values.elements)
  const [val, setVal] = useState(props.values.elements.join(' '))

  const [error, setError] = useState('')

  function handleTypeChange(e: any, value: any) {
    props.setType(value)
  }

  useDebounce(
    () => {
      if (val) {
        dataSchema.isValid({ data: val }).then((valid: boolean) => {
          if (valid) {
            const changedData = val.split(/[\s,]/)
            props.setData(changedData)
            setError('')
          } else {
            setError('Invalid Data')
          }
        })
      }
    },
    200,
    [val],
  )

  return (
    <Box sx={{ paddingY: 1 }}>
      <FormWrapper style={{ padding: 16, display: 'flex' }} submitHandler={props.save}>
        {() => (
          <>
            <Autocomplete
              disablePortal
              id="combo-box-demo"
              options={typeOptions}
              defaultValue={props.values.type}
              onChange={handleTypeChange}
              sx={{ width: 150, marginX: 1 }}
              renderInput={(params) => <TextField label="Type" placeholder="Bar" {...params} />}
            />
            <TextField
              defaultValue={val}
              placeholder="1 2 4 5"
              error={error !== ''}
              helperText={error}
              label="Data"
              multiline
              sx={{ marginX: 1 }}
              onChange={(e) => setVal(e.target.value)}
            />
            <Button type="submit" sx={{ marginY: 0.9, height: '37px' }} variant="outlined">
              Save
            </Button>
          </>
        )}
      </FormWrapper>
    </Box>
  )
}
