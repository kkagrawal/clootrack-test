import generators from '../generators'
import DataModel from '../models/data.model'

export function getOptions(chartData: DataModel) {
  if (generators.hasOwnProperty(chartData.type.toLocaleLowerCase())) {
    return generators[chartData.type].options(chartData)
  } else {
    return {}
  }
}
