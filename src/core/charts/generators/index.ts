import { IGenerator } from '../interfaces/generator.interface'
import barGenerator from './bar.generator'
import pieGenerator from './pie.generator'

const generator: Record<string, IGenerator> = {
  bar: barGenerator,
  pie: pieGenerator,
}

export default generator
