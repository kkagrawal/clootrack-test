import { IGenerator } from '../interfaces/generator.interface'
import DataModel from '../models/data.model'

const generator: IGenerator = {
  getDataModel(data: any) {
    return new DataModel('bar', data)
  },
  options(chartData: DataModel) {
    return {
      xAxis: { type: 'category', data: chartData.columns },
      yAxis: { type: 'value' },
      series: [
        {
          type: 'bar',
          data: chartData.values,
        },
      ],
    }
  },
}

export default generator
