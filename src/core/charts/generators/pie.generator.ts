import { IGenerator } from '../interfaces/generator.interface'
import DataModel from '../models/data.model'

const generator: IGenerator = {
  getDataModel(data: any) {
    return new DataModel('pie', data)
  },
  options(chartData: DataModel) {
    console.log('pie generator called')
    return {
      tooltip: {
        trigger: 'item',
      },
      series: [
        {
          type: 'pie',
          radius: '50%',
          label: false,
          data: chartData.toCollection(),
          emphasis: {
            itemStyle: {
              shadowBlur: 10,
              shadowOffsetX: 0,
              shadowColor: 'rgba(0, 0, 0, 0.5)',
            },
          },
        },
      ],
    }
  },
}

export default generator
