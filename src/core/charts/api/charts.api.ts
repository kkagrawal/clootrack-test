import envConfig from '@/config/env.config'

export function fetchAllData() {
  return fetch(envConfig.apiUrl).then((res) => res.json())
}

export default { fetchAllData }
