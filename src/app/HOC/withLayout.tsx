export function withLayout(LayoutComponent: any, PageComponent: any) {
  return (props: any) => (
    <LayoutComponent {...props}>
      <PageComponent {...props} />
    </LayoutComponent>
  )
}
