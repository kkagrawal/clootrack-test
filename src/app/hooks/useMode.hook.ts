import { useMediaQuery } from '@mui/material'

export const useMode = (defaultMode: 'light' | 'dark' | 'system' = 'light') => {
  if (defaultMode == 'system') {
    const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)')
    return prefersDarkMode ? 'dark' : 'light'
  }
  return defaultMode
}
