import './App.css'

import { ThemeProvider } from '@mui/material'

import themeConfig from '@/config/theme.config'
import IndexPage from '@/pages/Index.page'

import { useMode } from './hooks/useMode.hook'

function App() {
  const mode = useMode('light')
  console.log(mode)

  return (
    <ThemeProvider theme={themeConfig(mode)}>
      <IndexPage></IndexPage>
    </ThemeProvider>
  )
}

export default App
