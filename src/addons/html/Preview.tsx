export default (props: any) => {
  return (
    <div>
      <iframe title={props.title || 'Preview'} srcDoc={props.value} src={props.url} />
      {props.previewSrc && <img src={props.previewSrc} alt="" />}
    </div>
  )
}
