export default {
  title: import.meta.env.VITE_SITE_TITLE,
  baseUrl: import.meta.env.BASE_URL,
  apiUrl:
    import.meta.env.VITE_API_URL?.toString() ||
    'https://s3-ap-southeast-1.amazonaws.com/he-public-data/chart2986176.json',
}
