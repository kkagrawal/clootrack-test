import legacy from '@vitejs/plugin-legacy'
import { loadEnv } from 'vite'
import { ViteAliases } from 'vite-aliases'
import { injectHtml, minifyHtml } from 'vite-plugin-html'
import svgr from 'vite-plugin-svgr'
import react from 'vite-preset-react'

// https://vitejs.dev/config/
export default ({ mode }) => {
  // @ts-ignore:
  const env = loadEnv(mode, process.cwd())
  return {
    build: {
      outDir: 'build',
    },
    plugins: [
      react({ removeDevtoolsInProd: true, injectReact: true }),
      ViteAliases(),
      svgr(),
      minifyHtml(),
      injectHtml({
        injectData: {
          title: env.VITE_SITE_TITLE || 'App',
        },
      }),
      legacy({
        targets: ['defaults', 'last 5 versions'],
      }),
    ],
  }
}
