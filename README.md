# Battle Arena

This is a project built using React + Typescript with Vite Bundler.

Important Components

# Setup Done

- Linting/Prettier
- config
- Material UI
  - Dark/Light Mode Setup
- Global State Management
- project Structure
  - ui
  - pages
  - core
  - utils
  - resources
- Routing
  - auth
  - Use Replace to avoid user from going to previous page.
  - Lazy Loading some components which will be loaded afterwards.

# Setup Pending

- Services which deals with API or Third Party Services
- Logging
- CI/CD
- RemoteConfig

# Project Structure.

# Naming Conventions
